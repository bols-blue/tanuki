package tanuki

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const createDeviceAuthResponseJSON = `[]`

type mockTransport struct {
	mockRoundTrip func(req *http.Request, config map[string]string) (*http.Response, error)
	config        map[string]string
}

func (m *mockTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	return m.mockRoundTrip(req, m.config)
}

func jsonFileRoundTrip(req *http.Request, config map[string]string) (*http.Response, error) {
	response := &http.Response{
		Header:     make(http.Header),
		Request:    req,
		StatusCode: http.StatusOK,
	}
	fmt.Printf("rul=%s\n", req.URL.Path)
	fmt.Printf("config=%v\n", config)
	fileName := strings.Replace(req.URL.Path[1:], "/", "_", -1)
	fmt.Printf("rul=%s\n", fileName)

	file, err := os.Open(config["testResoceRootPath"] + "/" + fileName + ".json")
	if err != nil {
		return &http.Response{
			Header:     make(http.Header),
			Request:    req,
			StatusCode: http.StatusBadRequest,
		}, nil
	}
	response.Header.Set("Content-Type", "application/json")
	response.Body = ioutil.NopCloser(bufio.NewReader(file))
	return response, nil
}

func CreateDefaultMockHttpClient(testResoceRootPath string) *http.Client {
	return &http.Client{Transport: &mockTransport{
		mockRoundTrip: jsonFileRoundTrip,
		config: map[string]string{
			"testResoceRootPath": testResoceRootPath,
		},
	}}
}
